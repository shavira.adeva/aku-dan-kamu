from django.db import models

# Create your models here.

class Account(models.Model):
    name = models.CharField(max_length = 30)
    birth = models.CharField(max_length = 20)
    gender = models.CharField(max_length = 12)
    Expertise = models.TextField()
    Description = models.TextField()
    Email = models.EmailField(default = 'bintangilham41@gmail.com')
    UrlImage = models.TextField()
# Keperluan form edit profile
