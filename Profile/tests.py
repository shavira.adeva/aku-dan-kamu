from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views  import index
from .models import Account
from .forms import EditForm
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import http.client
from urllib.parse import urlparse
# Create your tests here.

class ProfileUnitTest(TestCase):

    #Test keberadaan url /profile
    def test_profile_url_is_exist(self):
        new_profile = Account.objects.create(name = 'Yumna Pratista Tastaftian', birth = '3 August', gender = 'Male', Expertise = 'Memancing, Berkuda, Basket',
                                             Description = 'Aku botak. Aku cinta padamu sampai kaki.', Email = 'yumnanaruto@gmail.com')

        profile = Account.objects.first()
        response = Client().get('/profile/')
        self.assertEqual(response.status_code,200)

    #Test keberadaan fungsi index
    def test_profile_using_index(self):
        found = resolve('/profile/')
        self.assertEqual(found.func, index)

    #Test model dapat dibuat
    def test_model_can_create_profile(self):
        new_profile = Account.objects.create(name = 'Yumna Pratista Tastaftian', birth = '3 August', gender = 'Male', Expertise = 'Memancing, Berkuda, Basket',
                                             Description = 'Aku botak. Aku cinta padamu sampai kaki.', Email = 'yumnanaruto@gmail.com')

        counting_all_data_object = Account.objects.all().count()
        self.assertEqual(counting_all_data_object, 1)

    # Test mengecek model sudah ada di html
    def test_account_in_html(self):
        new_profile = Account.objects.create(name = 'Yumna Pratista Tastaftian', birth = '3 August', gender = 'Male', Expertise = 'Memancing, Berkuda, Basket',
                                             Description = 'Aku botak. Aku cinta padamu sampai kaki.', Email = 'yumnanaruto@gmail.com')
        profile = Account.objects.first()

        data_account = {'name' :'Yumna Pratista Tastaftian' , 'birth' : '3 August', 'gender' : 'Male', 'Expertise' : 'Memancing, Berkuda, Basket',
                           'Description' :'Aku botak. Aku cinta padamu sampai kaki.', 'Email' : 'yumnanaruto@gmail.com'}

        response = Client().post('/profile/', data_account)

        self.assertEqual(response.status_code, 200)

        response_2 = Client().get('/profile/')
        self.assertEqual(response_2.status_code, 200)

        html_response = response_2.content.decode('utf8')

        for key,data in data_account.items():
            self.assertIn(data, html_response)

    def test_account_use_html_templates(self):
        new_profile = Account.objects.create(name = 'Yumna Pratista Tastaftian', birth = '3 August', gender = 'Male', Expertise = 'Memancing, Berkuda, Basket',
                                             Description = 'Aku botak. Aku cinta padamu sampai kaki.', Email = 'yumnanaruto@gmail.com')

        profile = Account.objects.first()

        response = Client().get('/profile/')
        self.assertTemplateUsed(response, 'profile_display.html')

    def test_account_use_edit_html_templates(self):
        new_profile = Account.objects.create(name = 'Yumna Pratista Tastaftian', birth = '3 August', gender = 'Male', Expertise = 'Memancing, Berkuda, Basket',
                                             Description = 'Aku botak. Aku cinta padamu sampai kaki.', Email = 'yumnanaruto@gmail.com')

        profile = Account.objects.first()

        response = Client().get('/profile/edit_profile/')
        self.assertTemplateUsed(response, 'edit_profile.html')

    def test_edit_profile_has_placeholder_and_css_classes(self):
        form = EditForm()
        self.assertIn('class="profile-input"', form.as_p())
        self.assertIn('id="id_name"', form.as_p())
        self.assertIn('class="profile-input-textarea', form.as_p())
        self.assertIn('id="id_Description', form.as_p())

    def test_form_validation_for_blank_items(self):
        form = EditForm(data={'name' :'' , 'birth' : '', 'gender' : '', 'Expertise' : '','Description' :'', 'Email' : '', 'UrlImage':''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['Description'],
            ["This field is required."]
        )
    def test_Profile_post_success_and_render_the_result(self):
        test = 'Anonymous'
        Account.objects.create(name = 'Yumna Pratista Tastaftian', birth = '3 August', gender = 'Male', Expertise = 'Memancing, Berkuda, Basket',
                                             Description = 'Aku botak. Aku cinta padamu sampai kaki.', Email = 'yumnanaruto@gmail.com', UrlImage = 'ajjsbassa')
        profile = Account.objects.first()
        response_post = Client().post('/profile/submit_profile/',{'name' : test , 'birth': test, 'gender' :test, 'Expertise' : test,'Description' :test, 'Email' :'bintangilham41@gmail.com', 'UrlImage':test})
        self.assertEqual(response_post.status_code, 302)

        redirects = response_post['location']
        self.assertEqual(redirects, '/profile/?new_profile=true')

        response= Client().get(redirects)
        html_response = response.content.decode('utf8')
        self.assertIn(test, html_response)

    def test_Profile_post_error_and_render_the_result(self):
        test = 'Anonymous'
        Account.objects.create(name = 'Yumna Pratista Tastaftian', birth = '3 August', gender = 'Male', Expertise = 'Memancing, Berkuda, Basket',
                                             Description = 'Aku botak. Aku cinta padamu sampai kaki.', Email = 'yumnanaruto@gmail.com', UrlImage = 'ajjsbassa')

        profile = Account.objects.first()
        response_post = Client().post('/profile/submit_profile/',{'name' :'' , 'birth' : '', 'gender' : '', 'Expertise' : '','Description' :'', 'Email' : '', 'UrlImage':''})
        self.assertEqual(response_post.status_code, 302)

        response= Client().get('/profile/')
        html_response = response.content.decode('utf8')
        self.assertNotIn(test, html_response)

    def get_server_status_code(self,url):
        res = urlparse(url)
        host = res[1]
        path = res[2]
        conn = http.client.HTTPConnection(host)
        conn.request('HEAD', path)
        return conn.getresponse().status

    def check_url(self,url):
        good_codes = [http.client.OK, http.client.FOUND, http.client.MOVED_PERMANENTLY]
        return self.get_server_status_code(url) in good_codes

    def test_url_image_is_exist(self):
        self.assertEqual(self.check_url('https://i.imgur.com/TWjqJsc.jpg'), True)
