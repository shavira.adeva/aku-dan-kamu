from django import forms

class EditForm(forms.Form):
    error_message ={
            'required' : 'Isi dengan data valid'
    }
    name_attrs = {
            'type' : 'text',
            'class' : 'profile-input',
            'placeholder': 'Masukan nama ....'
    }
    birth_attrs = {
            'type':'text',
            'class': 'profile-input',
            'placeholder': 'Format: tanggal bulan'
    }
    gender_attrs = {
            'type':'text',
            'class': 'profile-input',
            'placeholder': 'Masukan jatidiri anda'
    }
    Expertise_attrs = {
            'type':'text',
            'class': 'profile-input',
            'placeholder': 'Contoh: Leadership, Negosiator, dll'
    }
    Description_attrs = {
            'type':'text',
            'cols': 50,
            'rows': 4,
            'class': 'profile-input-textarea',
            'placeholder': 'Kamu seperti apa?'
    }
    Email_attrs = {
            'type':'text',
            'class': 'profile-input',
            'placeholder': 'xxxx@xx.com'
    }
    ImageUrl_attrs = {
            'type':'text',
            'cols': 50,
            'rows': 1,
            'class': 'profile-input-textarea',
            'placeholder': 'Url foto kamu'
    }
        
    name = forms.CharField(label  ='',max_length = 50, required =True, widget = forms.TextInput(attrs =name_attrs))
    birth = forms.CharField(label  ='',max_length = 20, required = True, widget = forms.TextInput(attrs =birth_attrs))
    gender = forms.CharField(label  ='',max_length = 12, required = True, widget = forms.TextInput(attrs = gender_attrs))
    Expertise = forms.CharField(label  ='',max_length = 100, widget = forms.TextInput(attrs =Expertise_attrs),required = True)
    Description = forms.CharField(label  ='',max_length = 250,required = True, widget=forms.Textarea(attrs=Description_attrs))
    Email = forms.EmailField(label  ='',required = True, widget = forms.TextInput(attrs = Email_attrs))
    UrlImage = forms.CharField(label  ='',required = True, widget=forms.Textarea(attrs=ImageUrl_attrs))
