from django.shortcuts import render
from django.http import HttpResponseRedirect, Http404
from .models import Account
from .forms import EditForm


# Create your views here.
response = {}
def index(request):
    html = 'profile_display.html'
    Account.objects.create(name = 'Yumna Pratista Tastaftian', birth = '3 August', gender = 'Male', Expertise = 'Memancing, Berkuda, Basket',
                           Description = 'Aku botak. Aku cinta padamu sampai kaki.', Email = 'yumnanaruto@gmail.com', UrlImage = 'https://i.imgur.com/TWjqJsc.jpg')
    profile = Account.objects.first()
    response['name'] = profile.name
    response['birth'] = profile.birth
    response['gender'] = profile.gender
    response['Expertise'] = profile.Expertise
    response['Description'] = profile.Description
    response['Email'] = profile.Email
    response['UrlImage'] = profile.UrlImage
    response['message'] = ''
    Account.objects.filter(name = 'Yumna Pratista Tastaftian').delete()
    if ('new_profile' in request.GET.dict().keys()):
        if (request.GET.dict()['new_profile'] == 'true'):
            response['message'] = 'Berhasil mengubah profile'
    return render(request, html, response)

def edit_profile(request):
    response['edit_profile'] = EditForm
    html = 'edit_profile.html'
    return render(request, html, response)

def submit_profile(request):
    form = EditForm(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        response['name'] = request.POST['name']
        response['birth'] = request.POST['birth']
        response['gender'] = request.POST['gender']
        response['Expertise'] = request.POST['Expertise']
        response['Description'] = request.POST['Description']
        response['Email'] = request.POST['Email']
        response['UrlImage'] = request.POST['UrlImage']
        Account.objects.all().delete()
        profile = Account(name = response['name'], birth = response['birth'], gender = response['gender'], Expertise = response['Expertise'],
                          Description = response['Description'], Email = response['Email'], UrlImage = response['UrlImage'])

        profile.save()
        return HttpResponseRedirect('/profile/?new_profile=true')
    else:
        return HttpResponseRedirect('/edit_profile/')
