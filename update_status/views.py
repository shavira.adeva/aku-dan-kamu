from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import Message_Form
from .models import Message

response = {}
def index(request):
    html = 'update_status/update_status.html'
    response['message_form'] = Message_Form
    return render(request, html, response)

def message_post(request):
    form = Message_Form(request.POST or None)
    if((request.method == 'POST') and form.is_valid()):
        response['message'] = request.POST['message']
        message = Message(message=response['message'])
        message.save()
        message = Message.objects.all()
        response['message'] = message
        return render(request, 'update_status/update_status.html', response)
    else:        
        return HttpResponseRedirect('/update-status/')

